﻿namespace VendingMachineKata.Models.Domain.Coin
{
    public class InvalidCoin : Coin
    {
        public InvalidCoin(double weight, double diameter)
        {
            Weight = weight;
            Value = 0;
            Diameter = diameter;
        }

        public override double Weight { get; }
        public override int Value { get; }
        public override double Diameter { get; }
    }
}