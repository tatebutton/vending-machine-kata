﻿namespace VendingMachineKata.Models.Domain.Coin
{
    public class Quarter : Coin
    {
        public override double Weight => 5.67;
        public override int Value => 25;
        public override double Diameter => 24.26;
    }
}
