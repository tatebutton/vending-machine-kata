﻿using System.Linq;
using VendingMachineKata.Models;
using VendingMachineKata.Models.Domain.Coin;
using VendingMachineKata.Models.Repository;

namespace VendingMachineKata.Services
{
    
    public class InsertedCoinService
    {

        private readonly ICoinRepository _coinRepository;

        public InsertedCoinService(ICoinRepository coinRepository)
        {
            _coinRepository = coinRepository;
        }


        public bool ValidateCoin(double weight, double diameter)
        {
            
            var isValid = _coinRepository.GetValidCoins().Any(c => c.Weight == weight && c.Diameter == diameter);
            return isValid;
        }

        public Coin GetCoin(double weight, double diameter)
        {
            return _coinRepository.GetValidCoins().First(c => c.Weight == weight && c.Diameter == diameter);
        }

        public void InsertCoin(double insertedCoinWeight, double insertedCoinDiameter)
        {
            var coinValid = ValidateCoin(insertedCoinWeight, insertedCoinDiameter);
            if (coinValid)
            {
                var coin = GetCoin(insertedCoinWeight, insertedCoinDiameter);
                _coinRepository.AddInsertedCoin(coin);
                Display.ShowAmount(_coinRepository.GetInsertedCoins().Sum(c => c.Value));
            }
            else
            {
                _coinRepository.AddReturnedCoin(new InvalidCoin(insertedCoinWeight, insertedCoinDiameter));
            }
            
        }
    }
}