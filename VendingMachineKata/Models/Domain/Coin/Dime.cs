﻿namespace VendingMachineKata.Models.Domain.Coin
{
    public class Dime : Coin
    {
        public override double Weight => 2.268;
        public override int Value => 10;
        public override double Diameter => 17.91;
    }
}
