﻿namespace VendingMachineKata.Models.Domain.Coin
{
    public interface ICoin
    {
        double Weight { get; }
        int Value { get; }
        double Diameter { get; }
    }

    public abstract class Coin : ICoin
    {
        public abstract double Weight { get;}
        public abstract int Value { get; }
        public abstract double Diameter { get; }
    }
}
