﻿namespace VendingMachineKata.Models.Domain.Coin
{
    public class Nickel : Coin
    {
        public override double Weight => 5;
        public override int Value => 5;
        public override double Diameter => 21.21;
    }
}
