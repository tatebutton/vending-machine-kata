using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SQLitePCL;
using VendingMachineKata.Models;
using VendingMachineKata.Models.Domain.Coin;
using VendingMachineKata.Models.Repository;
using VendingMachineKata.Services;

namespace VendingMachineKataTests
{
    [TestClass]
    public class InsertedCoinServiceTests
    {
        private InsertedCoinService _sut;
        private Mock<ICoinRepository> _coinRepo;

        [TestInitialize]
        public void Init()
        {
            _coinRepo = new Mock<ICoinRepository>();
            _coinRepo.Setup(cr => cr.GetValidCoins()).Returns(new List<Coin>()
            {
                new Dime(),
                new Nickel(),
                new Quarter()
            });
            
            _coinRepo.Setup(r => r.GetInsertedCoins()).Returns(new List<Coin>(){new Quarter(), new Nickel()});
            
            _sut = new InsertedCoinService(_coinRepo.Object);
        }
        
        [TestMethod]
        public void WhenQuarterIsValidated_ReturnTrue()
        {
            var coin = new Quarter();
            
            var result = _sut.ValidateCoin(coin.Weight, coin.Diameter);
            Assert.AreEqual(true,result);
        }

        [TestMethod]
        public void WhenValidCoinValidatedReturnTrue()
        {
            var coin = new Nickel();
            var result = _sut.ValidateCoin(coin.Weight, coin.Diameter);
            Assert.AreEqual(true,result);
        }
        

        [TestMethod]
        public void WhenInvalidCoinIsValidated_ReturnsFalse()
        {
            var weight = 1;
            var diameter = 1;
            var result = _sut.ValidateCoin(weight, diameter);
            Assert.AreEqual(false,result);
        }

        [TestMethod]
        public void WhenGetCoinCalledWithNickleProperties_ReturnsNickle()
        {
            var coin = new Nickel();
            var result = _sut.GetCoin(coin.Weight, coin.Diameter);
            Assert.IsInstanceOfType(result,typeof(Nickel));
        }

        [TestMethod]
        public void WhenQuarterIsInserted_InsertMethodIsCalled()
        {
            var coin = new Quarter();
            _sut.InsertCoin(coin.Weight, coin.Diameter);
            _coinRepo.Verify(r => r.AddInsertedCoin(It.IsAny<Coin>()));
        }

        [TestMethod]
        public void WhenNickIsInsertedAndQuarterInsertedExists_DisplayWillShow30Cents()
        {
           
            var coin = new Quarter();
            _sut.InsertCoin(coin.Weight, coin.Diameter);
            Assert.AreEqual(30, _coinRepo.Object.GetInsertedCoins().Sum(x => x.Value));
            Assert.AreEqual("30", Display.DisplayValue);
        }
        
        [TestMethod]
        public void WhenNoInsertedCoinsExist_InsertCoinDisplayWillShow()
        {
            
            _coinRepo.Setup(r => r.GetInsertedCoins()).Returns(new List<Coin>());
            _sut = new InsertedCoinService(_coinRepo.Object);
//            Assert.AreEqual("INSERT COIN", Display.DisplayValue);
        }
        
        [TestMethod]
        public void WhenNoInsertedCoinsExistAndInvalidCoinInserted_InsertCoinDisplayWillShow()
        {
            
            _coinRepo.Setup(r => r.GetInsertedCoins()).Returns(new List<Coin>());
            _sut = new InsertedCoinService(_coinRepo.Object);
           _sut.InsertCoin(11,12);
//           Assert.AreEqual("INSERT COIN", Display.DisplayValue);
        }

        [TestMethod]
        public void WhenInvalidCoinInserted_CoinPlacedInCoinReturn()
        {
            var coin = new Mock<Coin>();
            coin.SetupGet(c => c.Diameter).Returns(5);
            coin.SetupGet(c => c.Weight).Returns(6);
            coin.SetupGet(c => c.Value).Returns(15);  
            _sut.InsertCoin(coin.Object.Weight,coin.Object.Diameter);
            _coinRepo.Verify(r => r.AddReturnedCoin(It.IsAny<InvalidCoin>()));
        }
        
    }
}