﻿using System;
using System.Collections.Generic;
using VendingMachineKata.Models.Domain.Coin;

namespace VendingMachineKata.Models.Repository
{
    public interface ICoinRepository
    {
        List<Coin> GetValidCoins();
        List<Coin> GetReturnedCoins();
        List<Coin> GetInsertedCoins();
        void AddInsertedCoin(Coin coin);
        void AddReturnedCoin(Coin coin);
        
    }


    public class CoinRepository : ICoinRepository
    {
        public CoinRepository()
        {
            
        }
        public List<Coin> GetValidCoins()
        {
            throw new NotImplementedException();
        }


        public List<Coin> GetReturnedCoins()
        {
            throw new NotImplementedException();
        }

        public List<Coin> GetInsertedCoins()
        {
            throw new NotImplementedException();
        }

        public void AddInsertedCoin(Coin coin)
        {
            throw new NotImplementedException();
        }

        public void AddReturnedCoin(Coin coin)
        {
            throw new NotImplementedException();
        }

     }
    
    
 }