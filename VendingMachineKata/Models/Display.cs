﻿namespace VendingMachineKata.Models
{
    public static class Display
    {
         static Display()
        {
            ShowInsertCoin();
        }
        public static string DisplayValue;

        public static void ShowInsertCoin()
        {
            DisplayValue = "INSERT COIN";
        }

        public static void ShowOutOfStock()
        {
            DisplayValue = "OUT OF STOCK";    
        }

        public static void ShowExactChangeRequired()
        {
            DisplayValue = "EXACT CHANGE";
        }

        public static void ShowAmount(int amount)
        {
            DisplayValue = amount.ToString();
        }

    }
}